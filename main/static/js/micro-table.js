"use strict";

//npm install babel-preset-es2015 babel-preset-react
//babel --presets es2015,react --watch jsx/ --out-dir js/

//React element render one product row.
var ProductRow = React.createClass({ displayName: "ProductRow",
    render: function render() {
        return React.createElement(
            "tr",
            null,
            React.createElement(
                "td",
                null,
                React.createElement(
                    "a",
                    { href: this.props.product.slug },
                    this.props.product.name
                )
            ),
            React.createElement(
                "td",
                null,
                this.props.product.description
            ),
            React.createElement(
                "td",
                null,
                this.props.product.price
            ),
            React.createElement(
                "td",
                null,
                this.props.product.likes
            )
        );
    }
});

//React header element. Make table sortable.
var Header = React.createClass({ displayName: "Header",
    sort: function sort() {
        this.props.onUserSort(this.refs.sortRandRef.getDOMNode().value);
    },
    render: function render() {

        //Set header like label.
        var char = '';
        if (this.props.sortRand !== null) {
            char = this.props.sortRand ? "▼" : "▲";
        }
        var likes = "Likes" + char;

        //Return table hrader.
        return React.createElement(
            "thead",
            null,
            React.createElement(
                "tr",
                null,
                React.createElement(
                    "th",
                    null,
                    "Name"
                ),
                React.createElement(
                    "th",
                    null,
                    "Description"
                ),
                React.createElement(
                    "th",
                    null,
                    "Price"
                ),
                React.createElement(
                    "th",
                    {
                        value: this.props.sortRand,
                        onClick: this.sort,
                        ref: "sortRandRef"
                    },
                    likes
                )
            )
        );
    }
});

//React pagging element.
var PagElement = React.createClass({ displayName: "PagElem",
    paging_action: function paging_action() {
        this.props.handlePaging(this.refs.pagingRef.getDOMNode().value);
    },
    render: function render() {
        return React.createElement(
            "li",
            {
                value: this.props.i,
                onClick: this.paging_action,
                ref: "pagingRef"
            },
            React.createElement(
                "a",
                null,
                this.props.i
            )
        );
    }
});

// React pagging elem.
var Pagination = React.createClass({ displayName: "Pagination",
    render: function render() {
        var rows = [];
        //var genId = function* elemMaker(max){
        //    var index=1;
        //    while(index<=max)
        //        yield index++;
        //}(this.props.paginationMax);

        var items = [1, 2, 3];
        console.log(this.props.paginationMax);
        //var items = new Array(this.props.paginationMax);
        items.forEach(function (item) {
            rows.push(React.createElement(PagElement, { i: item, handlePaging: this.props.handlePaging }));
        }.bind(this));
        return React.createElement(
            "ul",
            { className: "pagination" },
            rows
        );
    }
});

var ProductTable = React.createClass({ displayName: "ProductTable",
    render: function render() {
        var rows = [];
        this.props.products.forEach(function (product) {
            rows.push(React.createElement(ProductRow, { product: product, key: product.name }));
        }.bind(this));
        var style = {};
        return React.createElement(
            "table",
            { style: style, className: "table table-hover" },
            React.createElement(Header, {
                sortRand: this.props.sortRand,
                onUserSort: this.props.handleSort
            }),
            React.createElement(
                "tbody",
                null,
                rows
            )
        );
    }
});

// Main elemnt have sortable table.
var FilterableProductTable = React.createClass({ displayName: "FilterableProductTable",
    getInitialState: function getInitialState() {
        return {
            sortRand: null,
            pagValue: 1,
            pagMax: 1
        };
    },
    handleUserInput: function handleUserInput(sortR) {
        this.setState({
            sortRand: sortR
        });
    },
    handleSort: function handleSort(sortR) {
        var sortVar = null;
        if (sortR == null) sortVar = true;else if (sortR == true) sortVar = false;
        console.log("sortVar" + sortVar);
        this.setState({
            sortRand: sortVar
        });
    },
    handlePagination: function handlePagination(pag_value) {
        console.log(pag_value);
        this.setState({
            pagValue: pag_value
        });
    },
    setPagMax: function setPagMax(n_elem_in_pag) {
        return this.props.products.length / n_elem_in_pag | 0;
    },
    render: function render() {
        var n_elem_in_pagination = 10;
        var rows = [];
        var sortCryteria = this.state.sortRand === null ? null : 'likes';
        var pr1 = _(this.props.products).sortByOrder([sortCryteria], [this.state.sortRand]).value();
        var sizeArr = pr1.length;
        var height = window.screen.height;
        var dataArr = pr1.slice((this.state.pagValue - 1) * n_elem_in_pagination, this.state.pagValue * n_elem_in_pagination);
        this.setPagMax(n_elem_in_pagination);
        var n_pag_max = this.setPagMax(n_elem_in_pagination);
        rows.push(React.createElement(ProductTable, {
            products: dataArr,
            sortRand: this.state.sortRand,
            handleSort: this.handleSort
        }));
        return React.createElement(
            "div",
            null,
            rows,
            React.createElement(Pagination, {
                handlePaging: this.handlePagination,
                paginationMax: n_pag_max
            })
        );
    }
});

//Product example.
//var PRODUCTS = [
//  {description: '$49.99', name: 'Football', price: 100, likes: 10},
//  {description: '$49.99', name: 'Football', price: 100, likes: 10},
//];

var pr1 = data();
React.render(React.createElement(FilterableProductTable, { products: pr1 }), document.getElementById('table'));