import datetime

from django.db import models
from django.contrib.auth.models import User


class ProductModel(models.Model):
    """

    ALTER TABLE product_model
        ALTER COLUMN created_at TYPE timestamp
        USING to_timestamp(created_at, 'DD-MON-YYY');
    """
    name = models.CharField(blank=False, max_length=50, unique=True, verbose_name='Name')
    slug = models.SlugField(verbose_name='Slug')
    description = models.TextField(verbose_name='Description')
    price = models.FloatField(verbose_name='Price')
    created_at = models.DateTimeField(auto_now_add=True)
    # created_at_date = models.DateField(auto_now_add=True)#, default=datetime.datetime.now().date())
    # created_at_time = models.TimeField(auto_now_add=True)#, default=datetime.datetime.now().time())
    modified_at = models.DateTimeField(auto_now=True)
    likes = models.ManyToManyField(User, related_name='likes', blank=True)

    @property
    def sum_likes(self):
        return self.likes.count()

    def __str__(self):
        return u'Name: %s, slug: %s' % (self.name, self.slug)

    class Meta:
        verbose_name = 'Product'
        verbose_name_plural = 'Products'


class CommentModel(models.Model):
    name = models.CharField(blank=False, max_length=50, verbose_name='Name')
    msg = models.TextField(blank=False, max_length=300, verbose_name='Comment')
    product = models.ForeignKey(ProductModel, verbose_name='Product')
    time = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return u'Name: %s, product: %s' % (self.name, self.product)

    class Meta:
        verbose_name = 'Comment'
        verbose_name_plural = 'Comments'