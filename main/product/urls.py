from django.conf.urls import url

from .views import IndexView, ProductListView, ProductItemView, LoginView

urlpatterns = [
    url(r'^$', IndexView.as_view(), name='index'),
    url(r'product/$', ProductListView.as_view(), name='product_list'),
    url(r'product/(?P<page_slug>\w+)', ProductItemView.as_view(), name='product_item'),
    url(r'login/', LoginView.as_view(), name='login')
]