from django.contrib import admin
from django.db import models
from django.forms import Textarea, TextInput
from django.utils.html import format_html

from .views import ProductModel, CommentModel


class BaseAdmin(admin.ModelAdmin):
    """Set form view settings."""
    formfield_overrides = {
        models.CharField: {'widget': TextInput(attrs={'size': '99'})},
        models.TextField: {'widget': Textarea(attrs={'rows': 3, 'cols': 100})},
    }


@admin.register(ProductModel)
class ProductAdmin(BaseAdmin):
    list_display = ('name', 'product_url')
    readonly_fields=('created_at', 'modified_at')

    def product_url(self, obj):
        """
        Create clickable admin-from field with link to prouct-obj.
        :param obj: current product obj.
        :return: link
        """
        return format_html("<a href='/product/{url}'>view on site</a>", url=obj.slug)

    product_url.short_description = "Product url"


@admin.register(CommentModel)
class CommentAdmin(BaseAdmin):
    list_display = ('product', 'name', 'msg', 'time')