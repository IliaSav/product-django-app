import json
import datetime
import pytz

from django.views.generic import TemplateView
from django.core.urlresolvers import reverse
from django.http import HttpResponseRedirect, HttpResponse
from django.shortcuts import render
from django.shortcuts import get_object_or_404
from django.contrib import messages

from .models import ProductModel, CommentModel
from forms import CommentForm


class IndexView(TemplateView):

    model = ProductModel
    template_name = "product/index.html"


class ProductListView(TemplateView):

    model = ProductModel
    template_name = 'product/list.html'

    def get(self, request, *args, **kwargs):
        products = ProductModel.objects.all()
        data = [{'description': p.description, 'name': p.name, 'likes': p.sum_likes, 'slug': p.slug, 'price': p.price}
                for p in products]
        return render(request, self.template_name, {'data': json.dumps(data)})


class ProductItemView(TemplateView):

    model = ProductModel
    template_name = 'product/item.html'

    def get_context_data(self, **kwargs):
        context = super(ProductItemView, self).get_context_data(**kwargs)
        product = get_object_or_404(ProductModel, slug=kwargs['page_slug'])
        product.total_likes = product.sum_likes
        context['product'] = product
        context['comments'] = CommentModel.objects.filter(product=context['product'],
                                                          time__gte=datetime.datetime.now(pytz.utc)-
                                                                    datetime.timedelta(days=1)).\
            order_by('-time')
        context['form'] = CommentForm()
        return context

    def post(self, request, *args, **kwargs):
        form = CommentForm(request.POST)

        # If like button click.
        if 'like_btn' == request.POST.get('btn'):
            user = request.user
            product = get_object_or_404(ProductModel, slug=kwargs['page_slug'])
            # User not auth.
            if not user.is_authenticated():
                return HttpResponse(
                    json.dumps({'data': 'not_auth'}),
                    content_type="application/json"
                )

            # User is auth.
            # User has already like this product (make dislike).
            if product.likes.filter(id=user.id).exists():
                product.likes.remove(user)
                msg = 'Dislike'

            # User like.
            else:
                product.likes.add(user)
                msg = 'Like'
            data = {'total_likes': product.sum_likes, 'data': msg}
            return HttpResponse(
                json.dumps(data),
                content_type="application/json"
            )

        # Add new comment. Login not required.
        elif form.is_valid():
            new_comment = form.save(commit=False)
            new_comment.product = ProductModel.objects.get(slug=kwargs['page_slug'])
            new_comment.save()
            messages.success(request, 'Comment add.')
            return HttpResponseRedirect(reverse('product_item', args=[kwargs['page_slug']]))

        return render(request, self.template_name, {'form': form})


class LoginView(TemplateView):

    template_name = 'product/login.html'
