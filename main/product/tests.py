import datetime
import pytz

from django.contrib.auth.models import AnonymousUser, User
from django.test import TestCase, RequestFactory
from .models import ProductModel, CommentModel
from .views import ProductItemView
from django.contrib.messages.storage.fallback import FallbackStorage

# Create your tests here.


def enable_masseges(request):
    """ Messages enable in tests."""
    setattr(request, 'session', 'session')
    messages = FallbackStorage(request)
    setattr(request, '_messages', messages)
    return request


class TestProductPage(TestCase):
    "Product test"
    def setUp(self):
        self.a=1
        self.factory = RequestFactory()
        self.user = User.objects.create_user(
            username='user1', email='user1@gmail.com', password='pass')
        self.name = 'name1'
        self.product = ProductModel.objects.create(name=self.name, description=self.name,
                                              slug=self.name, price=10.23)

    def test_user_auth_comment(self):
        "Test comments with autj user"
        kwargs = dict(page_slug=self.product.slug)
        request = self.factory.post('/product/',{'msg':'msg', 'name':'name'})

        #set auth user
        request.user = self.user

        request=enable_masseges(request)

        # get response
        end_comments = CommentModel.objects.filter(product=self.product,
                                                          time__gte=datetime.datetime.now(pytz.utc)-
                                                                    datetime.timedelta(days=1)).\
            order_by('-time')

        response = ProductItemView.as_view()(request, **kwargs)
        # response = self.factory.get('/product/'+self.product.slug)

        self.assertEqual(1, end_comments.count())

    def test_anonym_comment(self):
        "Test comments with anonym user"
        kwargs = dict(page_slug=self.product.slug)
        request = self.factory.post('/product/',{'msg':'msg', 'name':'name'})

        #set anonym user
        request.user = AnonymousUser()

        request=enable_masseges(request)

        # get response
        end_comments = CommentModel.objects.filter(product=self.product,
                                                          time__gte=datetime.datetime.now(pytz.utc)-
                                                                    datetime.timedelta(days=1)).\
            order_by('-time')

        response = ProductItemView.as_view()(request, **kwargs)

        self.assertEqual(1, end_comments.count())

    def test_auth_likes(self):
        "Test likes with auth user."
        kwargs = dict(page_slug=self.product.slug)
        request = self.factory.post('/product/',{'btn':'like_btn'})

        #set auth user
        request.user = self.user

        request=enable_masseges(request)

        response = ProductItemView.as_view()(request, **kwargs)

        self.assertEqual(1, self.product.sum_likes)

    def test_anonym_likes(self):
        "Test likes with anonim user."
        kwargs = dict(page_slug=self.product.slug)
        request = self.factory.post('/product/',{'btn':'like_btn'})

        #set anonim user
        request.user = AnonymousUser()

        request=enable_masseges(request)
        response = ProductItemView.as_view()(request, **kwargs)

        self.assertEqual(0, self.product.sum_likes)
