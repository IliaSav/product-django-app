from django.forms import ModelForm
from models import CommentModel


class CommentForm(ModelForm):
    class Meta:
        model = CommentModel
        fields = ['name', 'msg']
        labels = {
            'name': 'Name',
            'msg': 'Message'
        }
        error_messages = {
            'name': {
                'max_length': "This name is too long.  Allowed up to 50 characters.",
            },
            'msg': {
                'max_length': "Message is too long. Allowed up to 300 characters.",
            }
        }