# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('product', '0003_auto_20160127_0024'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='productmodel',
            name='modified_at',
        ),
        migrations.AlterField(
            model_name='productmodel',
            name='created_at',
            field=models.DateTimeField(auto_now_add=True),
        ),
    ]
