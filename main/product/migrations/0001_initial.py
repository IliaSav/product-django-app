# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='ProductModel',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(unique=True, max_length=50, verbose_name=b'Name')),
                ('slug', models.CharField(unique=True, max_length=50, verbose_name=b'Slug')),
                ('description', models.TextField(verbose_name=b'Description')),
                ('price', models.FloatField(verbose_name=b'Price')),
                ('created_at', models.TimeField(default=datetime.time(0, 20, 1, 842000), verbose_name=b'Created at', editable=False)),
                ('modified_at', models.TimeField(default=datetime.time(0, 20, 1, 842000), verbose_name=b'Modified at')),
            ],
            options={
                'verbose_name': 'Product',
                'verbose_name_plural': 'Products',
            },
        ),
    ]
