# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('product', '0005_productmodel_modified_at'),
    ]

    operations = [
        migrations.AddField(
            model_name='productmodel',
            name='likes',
            field=models.IntegerField(default=0),
        ),
    ]
