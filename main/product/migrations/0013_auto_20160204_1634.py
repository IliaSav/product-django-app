# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime


class Migration(migrations.Migration):

    dependencies = [
        ('product', '0012_auto_20160204_0159'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='productmodel',
            name='created_at',
        ),
        migrations.AddField(
            model_name='productmodel',
            name='created_at_date',
            field=models.DateField(default=datetime.date(2016, 2, 4), auto_now_add=True),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='productmodel',
            name='created_at_time',
            field=models.TimeField(default=datetime.time(16, 34, 4, 203000), auto_now_add=True),
            preserve_default=False,
        ),
    ]
