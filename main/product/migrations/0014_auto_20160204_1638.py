# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime


class Migration(migrations.Migration):

    dependencies = [
        ('product', '0013_auto_20160204_1634'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='productmodel',
            name='created_at_date',
        ),
        migrations.RemoveField(
            model_name='productmodel',
            name='created_at_time',
        ),
        migrations.AddField(
            model_name='productmodel',
            name='created_at',
            field=models.DateTimeField(default=datetime.datetime(2016, 2, 4, 16, 38, 56, 971000), auto_now_add=True),
            preserve_default=False,
        ),
    ]
