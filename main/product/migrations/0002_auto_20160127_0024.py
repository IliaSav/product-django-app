# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime


class Migration(migrations.Migration):

    dependencies = [
        ('product', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='productmodel',
            name='created_at',
            field=models.DateTimeField(default=datetime.datetime(2016, 1, 27, 0, 24, 14, 333000), verbose_name=b'Created at', editable=False),
        ),
        migrations.AlterField(
            model_name='productmodel',
            name='modified_at',
            field=models.DateTimeField(default=datetime.datetime(2016, 1, 27, 0, 24, 14, 333000), verbose_name=b'Modified at', editable=False),
        ),
    ]
