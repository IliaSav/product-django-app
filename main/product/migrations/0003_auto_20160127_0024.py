# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime


class Migration(migrations.Migration):

    dependencies = [
        ('product', '0002_auto_20160127_0024'),
    ]

    operations = [
        migrations.AlterField(
            model_name='productmodel',
            name='created_at',
            field=models.DateTimeField(default=datetime.datetime(2016, 1, 26, 22, 24, 58, 844000), verbose_name=b'Created at', editable=False),
        ),
        migrations.AlterField(
            model_name='productmodel',
            name='modified_at',
            field=models.DateTimeField(default=datetime.datetime(2016, 1, 26, 22, 24, 58, 844000), verbose_name=b'Modified at', editable=False),
        ),
    ]
