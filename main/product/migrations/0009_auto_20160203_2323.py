# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('product', '0008_auto_20160201_1253'),
    ]

    operations = [
        migrations.CreateModel(
            name='Likes',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('user_id', models.IntegerField(verbose_name=b'User id')),
                ('product_id', models.IntegerField(verbose_name=b'Product_id')),
            ],
        ),
        migrations.AlterField(
            model_name='productmodel',
            name='likes',
            field=models.IntegerField(default=0, blank=True),
        ),
        migrations.AlterUniqueTogether(
            name='likes',
            unique_together=set([('user_id', 'product_id')]),
        ),
    ]
